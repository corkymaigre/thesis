/*-------------------------------------------------------------------------------------------------------------------------*/
#include "stdafx.h"																	// include Visual Studio header file
extern "C" {																		// use extern "C" to include C-language libs
#include "sqlite3.h"																// include SQLite relational database
#include "unqlite.h"																// include UnQLite key-value database
}
/*-------------------------------------------------------------------------------------------------------------------------*/
#undef PREPARE_QUERY																// define to use prepared queries
#undef CREATE_SQLITE_DATABASE														// define to create and fill the SQLite relational database
#undef CREATE_UNQLITE_DATABASE														// define to create and fill the UnQLite key-value database
#undef DEBUG
#define TAG_FORMAT "YYYY-MM-DD HH:MM:SS"											// define the format of the tag
#define TAG_NAME "history"															// define the name of the tag
#define FALSE 0																		// define the boolean value for FALSE
#define TRUE 1																		// define the boolean value for TRUE
/*-------------------------------------------------------------------------------------------------------------------------*/
int tag_flag = FALSE;																// (int) flag to know if the tag is catched
std::string tag_value = "";															// (string) value of the tag
int tag_value_length;																// (int) length of the value of the tag
/*-------------------------------------------------------------------------------------------------------------------------*/
static int callback(void *NotUsed, int argc, char **argv, char **azColName)			// func: format the data output from SQLite
{
	int i;
	for (i = 0; i<argc; i++)
	{
		printf("%s: %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	return 0;
}
/*-------------------------------------------------------------------------------------------------------------------------*/
void check_tag(std::string&);														// func: check the tag in SQL
/*-------------------------------------------------------------------------------------------------------------------------*/
int main()																			// func: main function
{
	sqlite3 *sqlite_db = NULL;														// (sqlite3*) pointer to SQLite database
	sqlite3_stmt *stmtsqlite_stmt = NULL;											// (sqlite3_stmt*) pointer to SQLite statement									
	char *zErrMsg = 0;																// (char*) pointer to error message
	int rc;																			// (int) response of database call
	std::string sql;																// (string) SQL query					
	
	#ifdef CREATE_UNQLITE_DATABASE													// CREATE_UNQLITE_DATABASE
	unqlite *unqlite_db;															// (unqlite*) pointer to UnQLite database
	std::string key;																// (string) UnQLite key [table_number|row_number|column_number|date]
	char* value;																	// (string) UnQLite value
	rc = unqlite_open(&unqlite_db, "unqlite.db", UNQLITE_OPEN_CREATE);				// func: open UnQLite database
	key = "2112017-06-11 08:00:00";													// (string) UnQLite key [table_number|row_number|column_number|date]
	value = "First";																// (string) UnQLite value
	rc = unqlite_kv_store(unqlite_db, key.c_str(), -1, value, strlen(value));		// func: store key and value in UnQLite database
	key = "2122017-06-11 08:00:00";													// (string) UnQLite key [table_number|row_number|column_number|date]
	value = "10";																	// (string) UnQLite value
	rc = unqlite_kv_store(unqlite_db, key.c_str(), -1, value, strlen(value));		// func: store key and value in UnQLite database
	key = "2212017-06-11 08:00:00";													// (string) UnQLite key [table_number|row_number|column_number|date]
	value = "Second";																// (string) UnQLite value
	rc = unqlite_kv_store(unqlite_db, key.c_str(), -1, value, strlen(value));		// func: store key and value in UnQLite database
	key = "2222017-06-11 08:00:00";													// (string) UnQLite key [table_number|row_number|column_number|date]
	value = "20";																	// (string) UnQLite value
	rc = unqlite_kv_store(unqlite_db, key.c_str(), -1, value, strlen(value));		// func: store key and value in UnQLite database
	key = "2312017-06-11 08:00:00";													// (string) UnQLite key [table_number|row_number|column_number|date]
	value = "Third";																// (string) UnQLite value
	rc = unqlite_kv_store(unqlite_db, key.c_str(), -1, value, strlen(value));		// func: store key and value in UnQLite database
	key = "2322017-06-11 08:00:00";													// (string) UnQLite key [table_number|row_number|column_number|date]
	value = "30";																	// (string) UnQLite value
	rc = unqlite_kv_store(unqlite_db, key.c_str(), -1, value, strlen(value));		// func: store key and value in UnQLite database
	unqlite_close(unqlite_db);														// func: close UnQLite database
	#endif																			// CREATE_UNQLITE_DATABASE

	rc = sqlite3_open("sqlite.db", &sqlite_db);										// func: open SQLite database
	if (rc)
	{
		fprintf(stderr, "Can't open database: %s", sqlite3_errmsg(sqlite_db));
	}
	else
	{
		fprintf(stdout, "Opened database successfully");
		std::cout << " - Press 'quit' or 'q' to exit the app...\n";

		#ifdef CREATE_SQLITE_DATABASE												// CREATE_SQLITE_DATABASE
		sql = "create table person(name varchar(25), age int);";					// (string) SQL query
		check_tag(sql);																// func: check tag in SQL
		rc = sqlite3_exec(sqlite_db, sql.c_str(), callback, 0, &zErrMsg, FALSE, ::tag_value.c_str(), ::tag_value_length);	// func: execute SQL
		sql = "insert into person values('corky',23);";								// (string) SQL query
		check_tag(sql);																// func: check tag in SQL
		rc = sqlite3_exec(sqlite_db, sql.c_str(), callback, 0, &zErrMsg, FALSE, ::tag_value.c_str(), ::tag_value_length);	// func: execute SQL
		sql = "insert into person values('jack',25);";								// (string) SQL query
		check_tag(sql);																// func: check tag in SQL
		rc = sqlite3_exec(sqlite_db, sql.c_str(), callback, 0, &zErrMsg, FALSE, ::tag_value.c_str(), ::tag_value_length);	// func: execute SQL
		sql = "insert into person values('jones',22);";								// (string) SQL query
		check_tag(sql);																// func: check tag in SQL
		rc = sqlite3_exec(sqlite_db, sql.c_str(), callback, 0, &zErrMsg, FALSE, ::tag_value.c_str(), ::tag_value_length);	// func: execute SQL
		#endif																		// CREATE_SQLITE_DATABASE

		#ifdef DEBUG																// DEBUG
			sql = "select * from person;";											// execute a SQL query to avoid 3 loops before reading
			rc = sqlite3_exec(sqlite_db, sql.c_str(), NULL, 0, &zErrMsg, FALSE, ::tag_value.c_str(), ::tag_value_length);	// data in OP_COLUMN when debug mode is on
		#endif																		// DEBUG

		while(1)
		{
			sql = "";																	// Reset SQL query
			tag_value = "";																// Reset value of the tag
			std::cout << "sqlite-corky> ";												// Display sqlite prompt command
			std::getline(std::cin, sql);												// Get SQL input
			if (sql == "quit" || sql == "q")											// If SQL is equals to 'quit' or 'q'
				break;																	// Then quit the loop
			check_tag(sql);																// Check SQL for the tag
			#ifndef PREPARE_QUERY														// PREPARE_QUERY (OFF)
			rc = sqlite3_exec(sqlite_db, sql.c_str(), callback, 0, &zErrMsg, ::tag_flag, ::tag_value.c_str(), ::tag_value_length);			// Execute SQL
			if (rc != SQLITE_OK)														// If SQL syntax is not OK
			{
				fprintf(stderr, "SQL error: %s\n", zErrMsg);							// Print the error message
				sqlite3_free(zErrMsg);													// Free the message
			}
			else																		// If SQL syntax is OK
			{
				fprintf(stdout, "Operation done successfully\n");						// Print confirmation message
			}
			#else																		// PREPARE_QUERY (ON)
			rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);					// Prepare the SQL query
			if (rc != SQLITE_OK)														// If SQL syntax is not OK
			{
				printf("ERROR : PREPARE \n");											// Print the error message
				break;																	// Quit the loop
			}
			rc = sqlite3_step(stmt);													// 
			if (rc == SQLITE_DONE || rc == SQLITE_OK)									// If SQL is OK and DONE
			{
				printf("ERROR : EXEC-PREPARE \n");										// Print the error message
				break;																	// Quit the loop
			}
			sqlite3_finalize(stmt);														// Finalize the SQL statement
			#endif																		// PREPARE_QUERY (END)
		}
		sqlite3_close(sqlite_db);																// Close database
	}
	return 0;																			// Quit program
}
/*-------------------------------------------------------------------------------------------------------------------------*/
void check_tag(std::string& sql)
{
	std::transform(sql.begin(), sql.end(), sql.begin(), ::tolower);						// Transform SQL to lowercase
	std::size_t pos = sql.find(TAG_NAME);												// Find the position of the tag
	if (pos != std::string::npos)														// If the tag is found
	{
		::tag_flag = TRUE;																	// Switch boolean flag to true
		::tag_value = sql.substr(pos+strlen(TAG_NAME)+1, strlen(TAG_FORMAT));				// Get the value of the tag (0000-00-00 00:00:00)
		::tag_value_length = strlen(TAG_FORMAT);
		sql.erase(pos-1);																// Remove the tag and its value from the SQL query
		sql += ";";																		// Add ';' at the end of the SQL query
	}
	else																				// If the tag is not found
	{
		::tag_flag = FALSE;																	// Switch boolean flag to false
	}																	
}
/*-------------------------------------------------------------------------------------------------------------------------*/
